源码地址：http://git.oschina.net/clmao/WebBrz

=======
## 在前面
本程序Demo采用C# WINFORM开发，是一个建议浏览器。

## 使用方法：<br/>
可以直接运行 \WebBrz\WebBrz\bin\Debug\WebBrz.exe

##预览图<br/>
![1](http://git.oschina.net/uploads/images/2014/1009/121427_4351c449_106233.jpeg)<br/>

## 联系方式
有问题请留言我的<a href="http://blog.clmao.com/?page_id=145" target="_blank">博客</a>
