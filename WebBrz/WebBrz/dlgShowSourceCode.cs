﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WebBrz
{
    public partial class dlgShowSourceCode : Form
    {
        public string Title;
        public string HtmlSourceCode;
        public dlgShowSourceCode()
        {
            InitializeComponent();
        }

        private void dlgShowSourceCode_Shown(object sender, EventArgs e)
        {
            this.Text = Title;
            txtSourceCode.Text = HtmlSourceCode;
        }
    }
}
